import os
import sys
import time
import RPi.GPIO as GPIO
from flask import Flask, render_template, send_from_directory, request
import subprocess


app = Flask(__name__)

GPIO.setmode(GPIO.BOARD)

APP_POWER_ON_GPIO_PORT = 38


class Relay():
    def __init__(self, name, port):
        self.name = name
        self.port = port
        self.state = "Off"

        GPIO.setup(self.port, GPIO.OUT)
        self.off()

    @property
    def state(self):
        return self.state

    def on(self):
        GPIO.output(self.port, True)
        self.state = "On"

    def off(self):
        GPIO.output(self.port, False)
        self.state = "Off"


class Group():
    def __init__(self, name, relays):
        self.name = name
        self.relays = relays


groups = [
    Group("Lights", [
        # K1
        Relay("Light 1", 29),

        # K2
        Relay("Light 2", 31),

        # K3
        Relay("Light 3", 32),

        # K4
        Relay("Decorative Lights", 33),
    ]),
    Group("Peripherals", [
        # K5
        Relay("Handlebar Heaters", 22),

        # K6
        Relay("Amplifier", 36),
    ]),
    Group("System", [
        # K7
        Relay("Fans", 35),

        # K8
        Relay("[Unallocated]", 40),
    ]),
]


@app.route('/static/<path:filename>')
def static_handler(filename):
    return send_from_directory('static', filename)


@app.route('/')
def index():
    return render_template('control.html', groups=groups)


@app.route('/system')
def system():
    temp = subprocess.Popen('sudo vcgencmd measure_temp', shell=True, stdout=subprocess.PIPE).stdout.readlines()[0]
    volts = subprocess.Popen('sudo vcgencmd measure_volts', shell=True, stdout=subprocess.PIPE).stdout.readlines()[0]
    return render_template('system.html', temperature=temp, volts=volts)


@app.route('/audio')
def audio():
    return render_template('audio.html')


@app.route('/control/<operation>/<int:gpio_port>')
def control(operation, gpio_port):
    for group in groups:
        for relay in group.relays:
            if relay.port == gpio_port:
                if operation == "Off":
                    relay.off()
                    return "Off"

                elif operation == "On":
                    relay.on()
                    return "On"

    return "Whoa there, cowboy."

@app.route('/shutdown')
def shutdown():
    GPIO.output(APP_POWER_ON_GPIO_PORT, False)
    func = request.environ.get('werkzeug.server.shutdown')
    if func is None:
        return 'Not running with the Werkzeug Server'
    func()
    os.system('poweroff')
    return 'Goodbye.'


if __name__ == '__main__':
    app.debug = True

    GPIO.setup(APP_POWER_ON_GPIO_PORT, GPIO.OUT)
    GPIO.output(APP_POWER_ON_GPIO_PORT, True)

    app.run(host='0.0.0.0', port=80)
